//
//  LoginViewController.swift
//  AppClimaGR2
//
//  Created by Santiago Pazmiño on 27/10/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    
    @IBOutlet weak var usernameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    @IBAction func entrarButton(_ sender: Any) {
        
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        
        switch (username, password) {
        case ("santy","santy"):
            performSegue(withIdentifier: "citySegue", sender: self)
          
        case ("santy", _):
            showAlert(message: "Contraseña incorrecta")
            print("Contraseña incorrecta")
        default:
            
           showAlert(message:"Usuario y contraseña incorrecto")
        }
    }
    
    private func showAlert(message:String){
        let alertController = UIAlertController(title: "ERROR", message: message, preferredStyle:.actionSheet)
        
        let accepAction = UIAlertAction(title: "Aceptar", style: .default) { (action) in
            self.usernameTextField.text = ""
            self.passwordTextField.text = ""
        }
        let accepActionCancel = UIAlertAction(title: "Cancelar", style: .default) { (action) in
            self.usernameTextField.text = ""
            self.passwordTextField.text = ""
        }
        alertController.addAction(accepAction)
        alertController.addAction(accepActionCancel)
        present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func invitadoButton(_ sender: Any) {
    }
}
