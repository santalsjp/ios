//
//  MyLocationViewController.swift
//  AppClimaGR2
//
//  Created by Santiago Pazmiño on 27/10/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit
import CoreLocation

class MyLocationViewController: UIViewController, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    var didGetWheater = false
    
    
    @IBOutlet weak var climaLabel: UILabel!
    
    @IBOutlet weak var cityLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse {
            locationManager.delegate = self
            locationManager.startUpdatingLocation()
            
        }
        
        

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func salirButtonPressed(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        let location = manager.location?.coordinate
        if !didGetWheater{
        getWheaterByLocation(
            lat: (location?.latitude)!,
            lon: (location?.longitude)!
        )
    }
    }
    
    
    private func getWheaterByLocation(lat: Double, lon:Double){
        let service = Servicio()
        service.getWeatherByLocation(lat: lat, lon: lon) { (weather, city) in
            DispatchQueue.main.async {
                self.climaLabel.text = weather
                self.cityLabel.text = city
                
            }
        }
        
        
        let urlString = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=6ab5226e43a66789fd06e599bf2ce368"
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let _ = error {
                return
            }
            
            let weatherData = data as! Data
            
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
               // print(weatherJson)
                let city = weatherDictionary["name"] ?? "error"
                print(city)
                let weatherArray = weatherDictionary["weather"] as! NSArray
                let weather = weatherArray[0] as! NSDictionary
                let weatherDesc = weather["description"] ?? "error"
                //completion(weatherDesc as! String)
                
            }
            catch{
                print("Error al parcear el Json")
            }
            
            
            
        }
        task.resume()
        
    }

    

    }

