//
//  Servicio.swift
//  AppClimaGR2
//
//  Created by Santiago Pazmiño on 10/11/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import Foundation

class Servicio {
    
    func getWeatherByCity(city:String, completion: @escaping (String) -> ()){
        let urlString = "http://api.openweathermap.org/data/2.5/weather?q=\(city)&appid=6ab5226e43a66789fd06e599bf2ce368"
        getWeather(urlString: urlString) { (weather, city) in
            completion(weather)
        }
        
    }
    
    func getWeatherByLocation(lat: Double, lon:Double, completion: @escaping (String, String?) -> ()){
        let urlString = "http://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&appid=6ab5226e43a66789fd06e599bf2ce368"
        getWeather(urlString: urlString) { (weather, city) in
            completion(weather, city)
            
        }
        
        
        
    }
    
    private func getWeather(urlString:String, completion: @escaping (String, String?) -> ()){
        
        let url = URL(string: urlString)
        let request = URLRequest(url: url!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            if let _ = error {
                return
            }
            
            let weatherData = data as! Data
            
            do {
                let weatherJson = try JSONSerialization.jsonObject(with: weatherData, options: [])
                let weatherDictionary = weatherJson as! NSDictionary
                let city = weatherDictionary["name"] ?? "error"
                print(city)
                let weatherArray = weatherDictionary["weather"] as! NSArray
                let weather = weatherArray[0] as! NSDictionary
                let weatherDesc = weather["description"] ?? "error"
                completion(weatherDesc as! String, city as! String)
                
                
            }
            catch{
                print("Error al parcear el Json")
            }
            
            
            
        }
        task.resume()
        
    }

        
    }

