//
//  ByCityViewController.swift
//  AppClimaGR2
//
//  Created by Santiago Pazmiño on 6/11/17.
//  Copyright © 2017 Santiago Pazmiño. All rights reserved.
//

import UIKit

class ByCityViewController: UIViewController {
    
    
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var cityTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

   
    @IBAction func ButtonConsultar(_ sender: Any) {
        
        getWheaterByCity(cityTextField.text!)
        
    }
    
    private func getWheaterByCity(_ city:String){
        let service = Servicio()
        service.getWeatherByCity(city: city) { (weather) in
            DispatchQueue.main.async {
                self.weatherLabel.text = weather
            }
        }
        
    
    
    

    }

}
